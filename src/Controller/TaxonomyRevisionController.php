<?php

namespace Drupal\taxonomy_revision_ui\Controller;

use Drupal\diff\Controller\PluginRevisionController;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;

/**
 * TaxonomyRevisionController.
 */
class TaxonomyRevisionController extends PluginRevisionController {

  /**
   * Generates an overview table of older revisions of a library item.
   *
   * @param \Drupal\taxonomy\Entity\Term $taxonomy_term
   *   A Taxonomy Term entity object.
   *
   * @return array
   *   An array as expected by drupal_render()
   */
  public function revisionOverview(Term $taxonomy_term) {
    return $this->formBuilder()->getForm('Drupal\taxonomy_revision_ui\Form\TaxonomyRevisionOverviewForm', $taxonomy_term);
  }

  /**
   * A table showing the differences between two Taxonomy Term revisions.
   *
   * @param \Drupal\taxonomy\Entity\Term $taxonomy_term
   *   The entity whose revisions are compared.
   * @param int $left_revision
   *   Vid of the revision from the left.
   * @param int $right_revision
   *   Vid of the revision from the right.
   * @param string $filter
   *   If $filter == 'raw' raw text is compared (including html tags)
   *   If $filter == 'raw-plain' markdown function is applied to the text
   *                  before comparison.
   *
   * @return array
   *   Table showing the diff between the taxonomy_term revisions.
   */
  public function compareTaxonomyRevisions(Term $taxonomy_term, $left_revision, $right_revision, $filter) {

    try {
      $storage = $this->entityTypeManager()->getStorage('taxonomy_term');
      $route_match = \Drupal::routeMatch();
      $left_revision = $storage->loadRevision($left_revision);
      $right_revision = $storage->loadRevision($right_revision);
      $build = $this->compareEntityRevisions($route_match, $left_revision, $right_revision, $filter);
      return $build;
    }
    catch (\Exception $e) {
      watchdog_exception('taxonomy_revision_ui', $e);
      return ['#markup' => 'An error occurred, check the logs.'];
    }

  }

  /**
   * Page title callback for Taxonomy Term revision view page.
   *
   * @param int $taxonomy_term_revision
   *   The Taxonomy Term revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($taxonomy_term_revision) {
    try {
      $taxonomy_term = $this->entityTypeManager()->getStorage('taxonomy_term')->loadRevision($taxonomy_term_revision);

      if ($taxonomy_term instanceof TermInterface) {
        return $this->t('Revision of %title from %date', [
          '%title' => $taxonomy_term->label(),
          '%date' => date('m/d/Y - H:i', $taxonomy_term->getRevisionCreationTime()),
        ]);
      }

      return $this->t('Unexistent Revision ID');
    }
    catch (\Exception $e) {
      watchdog_exception('taxonomy_revision_ui', $e);
    }

  }

  /**
   * Displays a Taxonomy Term revision.
   *
   * @param int $taxonomy_term_revision
   *   The Taxonomy Term revision ID.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($taxonomy_term_revision) {
    $taxonomy_term = $this->entityTypeManager()->getStorage('taxonomy_term')->loadRevision($taxonomy_term_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('taxonomy_term');

    return $view_builder->view($taxonomy_term);
  }

}
