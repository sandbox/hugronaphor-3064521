<?php

namespace Drupal\taxonomy_revision_ui\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Taxonomy Term revision.
 *
 * @internal
 */
class TaxonomyRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Taxonomy Term revision.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $revision;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new TaxonomyRevisionDeleteForm.
   *
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_revision_ui_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.taxonomy_term.version_history', ['taxonomy_term' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $taxonomy_term_revision = NULL) {
    $this->revision = $this->termStorage->loadRevision($taxonomy_term_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->termStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('taxonomy_term')->notice('@type: deleted %title revision %revision.', [
      '@type' => $this->revision->bundle(),
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);

    $this->messenger()
      ->addStatus($this->t('Revision from %revision-date of @type %title has been deleted.', [
        '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
        '@type' => $this->getEntityBundleLabel($this->revision),
        '%title' => $this->revision->label(),
      ]));
    $form_state->setRedirect(
      'entity.taxonomy_term.canonical',
      ['taxonomy_term' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT revision_id) FROM {taxonomy_term_field_revision} WHERE tid = :tid', [':tid' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.taxonomy_term.version_history',
        ['taxonomy_term' => $this->revision->id()]
      );
    }
  }

  /**
   * Helper function to get the Entity's bundle label if available.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The Entity object.
   *
   * @return string|null
   *   The Entity's bundle or NULL if not available.
   */
  private function getEntityBundleLabel(ContentEntityInterface $entity) {

    try {
      $bundleTypeId = $entity->getEntityType()->getBundleEntityType();
      $bundleLabel = $this->entityTypeManager
        ->getStorage($bundleTypeId)
        ->load($entity->bundle())
        ->label();
      return $bundleLabel;
    }
    catch (\Exception $e) {
      watchdog_exception('entity_bundle_label_get', $e);
    }

  }

}
