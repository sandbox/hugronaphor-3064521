<?php

namespace Drupal\taxonomy_revision_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines a confirmation form to confirm reverting to a term revision by id.
 */
class TaxonomyRevisionRevertForm extends ConfirmFormBase {

  /**
   * The Taxonomy Term revision.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $revision;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new TaxonomyRevisionRevertForm.
   *
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, TimeInterface $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_revision_ui_revert_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to revert to the revision from %revision-date?', ['%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.taxonomy_term.version_history', ['taxonomy_term' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Revert');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $taxonomy_term_revision = NULL) {
    $this->revision = $this->termStorage->loadRevision($taxonomy_term_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $original_revision_timestamp = $this->revision->getRevisionCreationTime();

    $this->revision = $this->prepareRevertedRevision($this->revision, $form_state);
    $this->revision->setRevisionLogMessage($this->t('Copy of the revision from %date.', [
      '%date' => $this->dateFormatter->format($original_revision_timestamp),
    ]));
    $this->revision->setRevisionUserId($this->currentUser()->id());
    $this->revision->setRevisionCreationTime($this->time->getRequestTime());
    $this->revision->setChangedTime($this->time->getRequestTime());
    $this->revision->save();

    $this->logger('taxonomy_term')->notice('@type: reverted %title revision %revision.', [
      '@type' => $this->revision->bundle(),
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()->addStatus(
      $this->t('@type %title has been reverted to the revision from %revision-date.', [
        '@type' => $this->getEntityBundleLabel($this->revision),
        '%title' => $this->revision->label(),
        '%revision-date' => $this->dateFormatter->format($original_revision_timestamp),
      ])
    );
    $form_state->setRedirect('entity.taxonomy_term.version_history', [
      'taxonomy_term' => $this->revision->id(),
    ]);
  }

  /**
   * Prepares a revision to be reverted.
   *
   * @param \Drupal\taxonomy\TermInterface $revision
   *   The revision to be reverted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The prepared revision ready to be stored.
   */
  protected function prepareRevertedRevision(TermInterface $revision, FormStateInterface $form_state) {
    $revision->setNewRevision();
    $revision->isDefaultRevision(TRUE);

    return $revision;
  }

  /**
   * Helper function to get the Entity's bundle label if available.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The Entity object.
   *
   * @return string|null
   *   The Entity's bundle or NULL if not available.
   */
  private function getEntityBundleLabel(ContentEntityInterface $entity) {

    try {
      $bundleTypeId = $entity->getEntityType()->getBundleEntityType();
      $bundleLabel = $this->entityTypeManager
        ->getStorage($bundleTypeId)
        ->load($entity->bundle())
        ->label();
      return $bundleLabel;
    }
    catch (\Exception $e) {
      watchdog_exception('entity_bundle_label_get', $e);
    }

  }

}
