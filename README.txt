INTRODUCTION

Taxonomy Term entity became a revisionable as of Drupal 8.7.0 ( https://www.drupal.org/node/2897789 )
while the UI for it won't be available at least until https://www.drupal.org/project/drupal/issues/2936995
This module enable the basic manipulation of revisions for Taxonomy Term entity.


CONFIGURATION

To have a properly displayed Diff you have to create a view mode with
the machine name 'diff' and enable all the fields you want to visualized when
comparing two revisions.


KNOWN ISSUES

- It's been testes on a non-translatable Entity type so it's not guaranteed that
  it will work on a multilingual entity.